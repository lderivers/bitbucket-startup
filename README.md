# Fruit Shinobi #
This was my final project in my game programming class.
Fruit pops up and you have to slice them to gain points. 
Vegetables also pop up and when you slice them you lose points (vegetables are icky)

## Installation ##

Inside LDeriversFinalProject/installer there is a setup.exe file.
Running this exe will install the game to your computer to be played.

## Contributing ##

* Visual Studio was used in the original development of the project.
* The Visual Studio solution file can be found at `LDeriversFinalProject\FinalProjectTemplate\LDerivers_Fruit_Shinobi`.
* Of course you can always use your desired IDE instead.

## License ##
My choice in license ultimatly came down to two, the MIT license and the 2-Clause BSD (aka FreeBSD). While both are similar, I decided to go with the MIT license. The MIT license is easier to understand for others with less of a background in legal terminology. While I am allowing anyone to do whatever they want with my code; Be it open or closed source, they must also retain the license and copyright notices. I did originally write the code they are using, so I should at least have my name included some where :). There is also limited liability, I also would not want to be responsible if anything bad should happen due to my code.