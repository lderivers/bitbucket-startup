﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectShell
{
    /// <summary>
    /// Background image and music used for the game
    /// </summary>
    class Background : DrawableGameComponent
    {
        Texture2D background;
        int backgroundWidth;
        int backgroundHeight;
        Song backgroundMusic;

        public Background(Game game) : base(game)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = Game.Services.GetService<SpriteBatch>();

            sb.Begin();

            sb.Draw(background,
                new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height),
                new Rectangle(0, 0, backgroundWidth, backgroundHeight),
                Color.White);

            sb.End();

            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            background = Game.Content.Load<Texture2D>("Images/background");
            backgroundWidth = background.Width;
            backgroundHeight = background.Height;

            backgroundMusic = Game.Content.Load<Song>("Sounds/background");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.3f;
            MediaPlayer.Play(backgroundMusic);
            base.LoadContent();
        }
    }
}
