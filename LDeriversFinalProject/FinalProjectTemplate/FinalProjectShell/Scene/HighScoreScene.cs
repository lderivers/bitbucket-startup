﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{
    public class HighScoreScene : GameScene
    {
        /// <summary>
        /// Creates a High score scene for the user.
        /// </summary>
        /// <param name="game"></param>
        public HighScoreScene(Game game) : base(game)
        {

        }

        /// <summary>
        /// A new HighScoreList is created
        /// </summary>
        public override void Initialize()
        {
            this.SceneComponents.Add(new HighScoreListComponent(Game));
            this.Hide();
            base.Initialize();
        }

        /// <summary>
        /// Checks if the Escape character is pressed. If it is the user is returned to the main menu.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (Enabled)
            {

                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    ((Game1)Game).HideAllScenes();
                    Game.Services.GetService<StartScene>().Show();
                }
            }
            base.Update(gameTime);
        }


    }
}

