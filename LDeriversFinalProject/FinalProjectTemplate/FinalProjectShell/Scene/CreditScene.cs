﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{
    public class CreditScene : GameScene
    {
        /// <summary>
        /// Displays the credits image to the user
        /// </summary>
        /// <param name="game"></param>
        public CreditScene(Game game) : base(game)
        {

        }

        public override void Initialize()
        {
            Texture2D texture = Game.Content.Load<Texture2D>("Images/creditImage");

            this.SceneComponents.Add(new TextComponent(Game, texture));
            this.Hide();
            base.Initialize();
        }


        /// <summary>
        /// Checks if the Escape character is pressed. If it is the user is returned to the main menu.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (Enabled)
            {

                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    ((Game1)Game).HideAllScenes();
                    Game.Services.GetService<StartScene>().Show();
                }
            }
            base.Update(gameTime);
        }


    }
}
