﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{
    class TextComponent : DrawableGameComponent
    {
        Texture2D texture;

        /// <summary>
        /// Drawable component to be used in scenes depending on what texture is provided
        /// </summary>
        /// <param name="game"></param>
        /// <param name="texture"></param>
        public TextComponent(Game game, Texture2D texture) : base(game)
        {
            this.texture = texture;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = Game.Services.GetService<SpriteBatch>();

            spriteBatch.Begin();
            spriteBatch.Draw(texture, Vector2.Zero, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
        

        protected override void LoadContent()
        {
            base.LoadContent();
        }
    }
}
