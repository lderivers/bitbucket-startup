﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{
    /// <summary>
    /// Restarts the game when ran.
    /// Removes and adds a new instance of an ActionScene to the game with everything back to defaults.
    /// Displays a game over screen with user score and instructions.
    /// </summary>
    public class GameOver : GameScene
    {
        Texture2D texture;
        ActionScene actionScene;
        bool gameOver;
        public int score;

        public GameOver(Game game, ActionScene actionScene) : base(game)
        {
            this.actionScene = actionScene;
            this.gameOver = false;
        }

        /// <summary>
        /// Loads the gameOverImage.
        /// If the service GameOver does not exist a new one is added.
        /// </summary>
        public override void Initialize()
        {
            texture = Game.Content.Load<Texture2D>("Images/gameOverImage");

            if (Game.Services.GetService<GameOver>() == null)
            {
                Game.Services.AddService<GameOver>(this);
            }

            this.SceneComponents.Add(new GameOverComponent(Game, texture, this));

            this.Hide();
            base.Initialize();
        }

        /// <summary>
        /// Removes all fruit and vegetable from the game.
        /// Removes the actionScene and GameTools.
        /// Displays instructions to the user.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                if (gameOver)
                {
                    foreach (GameComponent component in Game.Components.ToList())
                    {
                        if (component is Fruit || component is Vegetable)
                        {
                            Game.Components.Remove(component);
                        }
                    }
                    Game.Services.RemoveService(typeof(ActionScene));
                    Game.Services.RemoveService(typeof(GameTools));

                    ((Game1)Game).HideAllScenes();


                    Game.Services.GetService<GameOver>().Show();

                    gameOver = false;

                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Ran when the time left in the game reaches zero, updates the score and sets the game to over
        /// </summary>
        /// <param name="score"></param>
        public void SetGameToOver(int score)
        {
            this.score = score;
            gameOver = true;
        }


    }
}
