﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{
    class HighScoreListComponent : DrawableGameComponent
    {
        SpriteFont SFScore;

        SaveScoreToFile hiScoreManager = new SaveScoreToFile();
        List<int> hiScoreList = new List<int>();
        string hiScores;

        public HighScoreListComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Draws the List of High Scores to the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = Game.Services.GetService<SpriteBatch>();

            spriteBatch.Begin();
            spriteBatch.DrawString(SFScore, $"High Score List: \n{hiScores}", new Vector2(Game.GraphicsDevice.Viewport.Width / 2 - SFScore.MeasureString($"High Score List: \n{hiScores}").X / 2, Game.GraphicsDevice.Viewport.Height / 2 - 400), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Loads the font to be used
        /// </summary>
        protected override void LoadContent()
        {
            SFScore = Game.Content.Load<SpriteFont>("Fonts\\Score");
            base.LoadContent();
        }

        /// <summary>
        /// Updates the highscore list
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            hiScores = string.Empty;
            hiScoreList = hiScoreManager.LoadFile();
            LoadHighScoreList();

            base.Update(gameTime);
        }

        /// <summary>
        /// Generates the high score string formatted on new lines to be displayed to the user
        /// </summary>
        /// <returns></returns>
        public string LoadHighScoreList()
        {
            foreach (int score in hiScoreList)
            {
                hiScores += score.ToString() + "\n";
            }
            return "0";
        }
        
    }
}