﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{

    class GameOverComponent : DrawableGameComponent
    {        
        SpriteFont SFScore;
        GameOver GameOverScene;
        public int score;
        
        /// <summary>
        /// Displays instructions to the user on how to restart the game along with their score.
        /// When the user presses space the game is restarted.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="texture"></param>
        /// <param name="parent"></param>
        public GameOverComponent(Game game, Texture2D texture, GameScene parent) : base(game)
        {
            GameOverScene = (GameOver)parent;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Draws the instructions and score to the screen.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = Game.Services.GetService<SpriteBatch>();

            spriteBatch.Begin();
            spriteBatch.DrawString(SFScore, $"Game over. Your Score: {score}. press space to play again.", new Vector2(Game.GraphicsDevice.Viewport.Width /2 - SFScore.MeasureString($"Game over. Your Score: {score}. press space to play again.").X/2, Game.GraphicsDevice.Viewport.Height/2-400), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Loads the SpriteFont to be used.
        /// </summary>
        protected override void LoadContent()
        {
            SFScore = Game.Content.Load<SpriteFont>("Fonts\\Score");
            base.LoadContent();
        }

        /// <summary>
        /// When the user presses space the GameOver scene is removed, the ActionScene is created and
        /// The starting Screen is then shown.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            score = GameOverScene.score;
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                Game.Services.RemoveService(typeof(GameOver));
                ActionScene actionScene = new ActionScene(Game);
                Game.Components.Add(actionScene);

                Game.Services.AddService<ActionScene>(actionScene);
                ((Game1)Game).HideAllScenes();
                Game.Services.GetService<StartScene>().Show();
            }
            base.Update(gameTime);
        }
    }
}
