﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace FinalProjectShell
{
    class HudString : DrawableGameComponent
    {
        protected HudLocation location;//Allows for changes of child classes, like score
        protected string displayString;
        string fontName;
        SpriteFont font;

        public HudString(Game game, string fontName, HudLocation screenLocation) : base(game)
        {
            this.fontName = fontName;
            this.location = screenLocation;
            DrawOrder = int.MaxValue;
            displayString = "Not set";
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = Game.Services.GetService<SpriteBatch>();

            sb.Begin();

            sb.DrawString(font, displayString, GetPosition(), Color.Red);

            sb.End();

            base.Draw(gameTime);
        }

        private Vector2 GetPosition()
        {
            Vector2 tempLocation = Vector2.Zero;

            float stringWidth = font.MeasureString(displayString).X;
            float stringHeight = font.MeasureString(displayString).Y;

            float screenWidth = Game.GraphicsDevice.Viewport.Width;
            float screenHeight = Game.GraphicsDevice.Viewport.Height;

            switch (location)
            {
                case HudLocation.TopLeft:
                    break;
                case HudLocation.TopCenter:
                    tempLocation.X = screenWidth / 2 - stringWidth / 2;
                    break;
                case HudLocation.TopRight:
                    tempLocation.X = screenWidth - stringWidth;
                    break;
                case HudLocation.CenterScreen:
                    tempLocation.X = screenWidth / 2 - screenWidth / 2;
                    tempLocation.Y = screenHeight / 2 - stringHeight / 2;
                    break;
                case HudLocation.BottomLeft:
                    tempLocation.Y = screenHeight - stringHeight;
                    break;
                case HudLocation.BottomCenter:
                    tempLocation.Y = screenHeight - stringHeight;
                    tempLocation.X = screenWidth / 2 - stringWidth / 2;
                    break;
                case HudLocation.BottomRight:
                    tempLocation.X = screenWidth - stringWidth;
                    tempLocation.Y = screenHeight - stringHeight;
                    break;
                default:
                    break;
            }
            return tempLocation;
        }

        protected override void LoadContent()
        {
            font = Game.Content.Load<SpriteFont>($"Fonts\\{fontName}");
            base.LoadContent();
        }
    }
}
