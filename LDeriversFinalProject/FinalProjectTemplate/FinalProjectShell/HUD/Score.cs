﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace FinalProjectShell
{
    class Score : HudString
    {
        int score = 0;

        public Score(Game game, string fontName, HudLocation screenLocation) 
            : base(game, fontName, screenLocation)
        {
            if(game.Services.GetService<Score>() == null)
            {
                game.Services.AddService<Score>(this);
            }

            displayString = $"Score: {score}";
        }

        public override void Update(GameTime gameTime)
        {
            displayString = $"Score: {score}";
            base.Update(gameTime);
        }

        public void AddToScore(int value)
        {
            score += value;
        }
    }
}
