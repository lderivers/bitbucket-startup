﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalProjectShell
{
    class DebugInfo : HudString
    {
        public DebugInfo(Game game, string fontName, HudLocation screenLocation) : base(game, fontName, screenLocation)
        {

        }

        public override void Update(GameTime gameTime)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("Debug Info: ");
            builder.AppendLine($"          Coins in Game:{GetCoinsInGame()}");
            builder.AppendLine($"          Coins in Scene:{GetCoinsInScene()}");
            displayString = builder.ToString();
                

            base.Update(gameTime);
        }

        private int GetCoinsInScene()
        {
            ActionScene scene = Game.Services.GetService<ActionScene>();

            return scene.GetCoinCount();
        }

        private int GetCoinsInGame()
        {
            int count = 0;

            foreach(GameComponent component in Game.Components)
            {
                if(component is Coin)
                {
                    count++;
                }
            }

            return count;
        }
    }
}
