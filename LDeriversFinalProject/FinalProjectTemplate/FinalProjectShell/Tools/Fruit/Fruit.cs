﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectShell
{
    /// <summary>
    /// The fruit spawn at the bottom with a direction of up,
    /// Once they reach the top of the screen the start going down until 
    /// they are despawned.
    /// </summary>
    enum Direction
    {
        Up,
        Down
    }

    /// <summary>
    /// A Fruit shown on the screen for the player to slide.
    /// </summary>
    class Fruit : DrawableGameComponent
    {
        GameTools gameTools;
        Texture2D texture;
        SoundEffect slice;
        int velocity = 0;

        const float VERTICAL_SPEED = 6.0f;
        Vector2 position = Vector2.Zero;
        Direction fruitDirection = Direction.Up;
        List<Rectangle> fruitHitBoxes = new List<Rectangle>();
        

        public Fruit(Game game) : base(game)
        {
        }

        /// <summary>
        /// Creates a new fruit from the FruitController.
        /// Gets gameTools to update score.
        /// A hitbox is created for each fruit.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="texture"></param>
        /// <param name="position"></param>
        /// <param name="velocity"></param>
        public Fruit(Game game, Texture2D texture, Vector2 position, int velocity) : base(game)
        {
            this.texture = texture;
            this.position = position;
            this.velocity = velocity;

            gameTools = game.Services.GetService<GameTools>();
            Rectangle rectangle = texture.Bounds;
        }

        /// <summary>
        /// Draws a new fruit.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = Game.Services.GetService<SpriteBatch>();

            sb.Begin();

            sb.Draw(texture, position, Color.White);

            sb.End();
            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Moves the fruit.
        /// If a fruit reaches the top the direction is changed to down.
        /// if a fruits direction is down, the fruit is going down.
        /// A fruit is constanly moving horizontally.
        /// If a fruit hits either side of the screen the direction is reversed.
        /// If a fruit goes past the bottom of the screen it is removed from the game.
        /// If a fruit is clicked on it is removed and the score is increased
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (this.position.Y <= 0) //When the fruit reaches the top of the screen it starts going down
            {
                this.fruitDirection = Direction.Down;
            }

            if(fruitDirection == Direction.Up) 
            {
                this.position.Y -= VERTICAL_SPEED;
            }
            else
            {
                this.position.Y += VERTICAL_SPEED;
            }

            this.position.X += velocity;

            if(position.X < 0 || position.X > GraphicsDevice.Viewport.Width - texture.Width)
            {
                velocity = velocity*-1;
            }

            if(position.Y - texture.Height > Game.GraphicsDevice.Viewport.Height) //Removes fruit when it goes off the screen
            {
                Game.Components.Remove(this);
            }

            Rectangle fruitHitBox = texture.Bounds;
            fruitHitBox.Location = this.position.ToPoint();

            MouseState mouseState = Mouse.GetState();
            var mousePosition = new Point(mouseState.X, mouseState.Y);

            if (fruitHitBox.Contains(mousePosition))
            {
                if(mouseState.LeftButton == ButtonState.Pressed)
                {
                    gameTools.UpdateScore(GameTools.FruitOrVegetable.Fruit);
                    slice.Play(volume:0.05f,pitch:0.0f,pan:0.0f);
                    
                    Game.Components.Remove(this);
                }
            }
            
            base.Update(gameTime);
        }

        /// <summary>
        /// Loads the slice sound to be played
        /// </summary>
        protected override void LoadContent()
        {
            slice = Game.Content.Load<SoundEffect>("Sounds/slice"); 

            base.LoadContent();
        }
    }
}
