﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectShell
{
    class FruitController : GameComponent
    {
        List<Texture2D> FruitList = new List<Texture2D>();

        GameScene parent;

        const int MAX_FRUIT = 15; //Max fruit to be able on the screen
        Random random = new Random();
        int numOfFruit = 0; //Number of fruit to be spawned
        const double FRUIT_SPAWN_INTERVAL = 1.5;//Fruit are spawned every 1.5 seconds
        double fruitTimer = 1.5; //Reset to 0 when a fruit is spawned, timer increases by game time.
        int velocity = 0;//Horizontal speed
        bool spawnLeft = true;//Determines if a fruit spawns on the left or right

        /// <summary>
        /// Creates a new FruitController, parent is the action scene
        /// </summary>
        /// <param name="game"></param>
        /// <param name="parent"></param>
        public FruitController(Game game, GameScene parent) : base(game)
        {
            this.parent = parent;
        }

        /// <summary>
        /// Loads all of the different fruit images
        /// </summary>
        public override void Initialize()
        {
            FruitList.Add(Game.Content.Load<Texture2D>("Images/banana"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/lemon"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/orange"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/peach"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/red-apple"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/red-cherry"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/strawberry"));
            FruitList.Add(Game.Content.Load<Texture2D>("Images/watermelon"));
            
            base.Initialize();
        }

        /// <summary>
        /// Returns an int of the number of fruit on the screen
        /// </summary>
        /// <returns></returns>
        public int FruitOnScreen()
        {
            int fruitCount = 0;

            foreach(GameComponent component in Game.Components)
            {
                if (component is Fruit)
                {
                    fruitCount++;
                }
            }
            return fruitCount;
        }

        /// <summary>
        /// If the number of fruit on the screen is less than the max number of fruit,
        /// a random number of fruit are spawned.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            fruitTimer += gameTime.ElapsedGameTime.TotalSeconds;
            if (fruitTimer >= FRUIT_SPAWN_INTERVAL)
            {
                fruitTimer = 0.0;

                if (FruitOnScreen() < MAX_FRUIT) { 
                    this.numOfFruit = random.Next(1, 4);
                    for (int i = 0; i <= numOfFruit; i++)
                    {
                        velocity = random.Next(3, 12);
                        
                        Vector2 startingPosition = GenerateRandomStartingPosition(ref velocity);

                        parent.AddComponent(new Fruit(Game, FruitList[random.Next(0, FruitList.Count)], startingPosition, velocity));
                    }
                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Swaps between fruit spawning on the left or right.
        /// If a fruit spawns on the right it moves left, if a fruit spawns on the left it moves right.
        /// </summary>
        /// <param name="velocity"></param>
        /// <returns></returns>
        public Vector2 GenerateRandomStartingPosition(ref int velocity)
        {
            if (spawnLeft)
            {
                spawnLeft = !spawnLeft;
                return new Vector2(random.Next(0, Game.GraphicsDevice.Viewport.Width / 2), Game.GraphicsDevice.Viewport.Height);
            }
            else
            {
                spawnLeft = !spawnLeft;
                velocity = -velocity;
                return new Vector2(random.Next(Game.GraphicsDevice.Viewport.Width / 2, Game.GraphicsDevice.Viewport.Width-200), Game.GraphicsDevice.Viewport.Height);
            }
        }
    }
}
