﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectShell
{
    class VegetableController : GameComponent
    {
        List<Texture2D> VegetableList = new List<Texture2D>();

        GameScene parent;

        const int MAX_VEGETABLES = 5;//Max vegetables to be able on the screen
        Random random = new Random();
        int numOfVegetable = 0;//Number of vegetables to be spawned
        const double VEGETABLE_SPAWN_INTERVAL = 1.5;//Vegetables are spawned every 1.5 seconds
        double VegetableTimer = 1.5;//Reset to 0 when a fruit is spawned, timer increases by game time.
        int velocity = 0; //Horizontal speed
        bool spawnLeft = true; //Determines if a fruit spawns on the left or right

        /// <summary>
        /// Creates a new VegetableController, parent is the action scene
        /// </summary>
        /// <param name="game"></param>
        /// <param name="parent"></param>
        public VegetableController(Game game, GameScene parent) : base(game)
        {
            this.parent = parent;
        }

        /// <summary>
        /// Loads all of the different vegetable images
        /// </summary>
        public override void Initialize()
        {
            VegetableList.Add(Game.Content.Load<Texture2D>("Images/broccoli"));
            VegetableList.Add(Game.Content.Load<Texture2D>("Images/carrot"));
            VegetableList.Add(Game.Content.Load<Texture2D>("Images/cucumber"));
            VegetableList.Add(Game.Content.Load<Texture2D>("Images/pepper"));
            VegetableList.Add(Game.Content.Load<Texture2D>("Images/tomato"));

            base.Initialize();
        }

        /// <summary>
        /// Returns an int of the number of vegetables on the screen
        /// </summary>
        /// <returns></returns>
        public int VegetableCount()
        {
            int vegetableCount = 0;

            foreach (GameComponent component in Game.Components)
            {
                if (component is Vegetable)
                {
                    vegetableCount++;
                }
            }
            return vegetableCount;
        }

        /// <summary>
        /// If the number of vegetable on the screen is less than the max number of vegetables,
        /// a random number of vegetable are spawned.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            int numVegetable = VegetableCount();

            VegetableTimer += gameTime.ElapsedGameTime.TotalSeconds;
            if (VegetableTimer >= VEGETABLE_SPAWN_INTERVAL)
            {
                VegetableTimer = 0.0;

                if (numVegetable < MAX_VEGETABLES)
                {
                    this.numOfVegetable = random.Next(1, 2);
                    for (int i = 0; i <= numOfVegetable; i++)
                    {
                        velocity = random.Next(3, 6);

                        Vector2 startingPosition = GenerateRandomStartingPosition(ref velocity);

                        parent.AddComponent(new Vegetable(Game, VegetableList[random.Next(0, VegetableList.Count)], startingPosition, velocity));
                    }
                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Swaps between vegetables spawning on the left or right.
        /// If a vegetable spawns on the right it moves left, if a vegetable spawns on the left it moves right.
        /// </summary>
        /// <param name="velocity"></param>
        /// <returns></returns>
        public Vector2 GenerateRandomStartingPosition(ref int velocity)
        {
            if (spawnLeft)
            {
                spawnLeft = !spawnLeft;
                return new Vector2(random.Next(0, Game.GraphicsDevice.Viewport.Width / 2), Game.GraphicsDevice.Viewport.Height);
            }
            else
            {
                spawnLeft = !spawnLeft;
                velocity = -velocity;
                return new Vector2(random.Next(Game.GraphicsDevice.Viewport.Width / 2, Game.GraphicsDevice.Viewport.Width - 200), Game.GraphicsDevice.Viewport.Height);
            }
        }
    }
}
