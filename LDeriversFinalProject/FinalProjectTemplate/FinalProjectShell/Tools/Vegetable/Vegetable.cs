﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectShell
{
    class Vegetable : DrawableGameComponent
    {
        GameTools gameTools;
        Texture2D texture;
        SoundEffect squish;
        int velocity = 0;

        float VERTICAL_SPEED = 6.0f;
        Vector2 position = Vector2.Zero;
        Direction vegetableDirection = Direction.Up;
        List<Rectangle> vegetableHitBoxes = new List<Rectangle>();


        public Vegetable(Game game) : base(game)
        {
        }

        public Vegetable(Game game, Texture2D texture, Vector2 position, int velocity) : base(game)
        {
            this.texture = texture;
            this.position = position;
            this.velocity = velocity;

            gameTools = game.Services.GetService<GameTools>();
            Rectangle rectangle = texture.Bounds;
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = Game.Services.GetService<SpriteBatch>();

            sb.Begin();

            sb.Draw(texture, position, Color.White);

            sb.End();
            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (this.position.Y <= 0) //When the vegetable reaches the top of the screen it starts going down
            {
                this.vegetableDirection = Direction.Down;
            }

            if (vegetableDirection == Direction.Up)
            {
                this.position.Y -= VERTICAL_SPEED;
            }
            else
            {
                this.position.Y += VERTICAL_SPEED;
            }

            this.position.X += velocity;

            if (position.X < 0 || position.X > GraphicsDevice.Viewport.Width - texture.Width)
            {
                velocity = velocity * -1;
            }

            if (position.Y - texture.Height > Game.GraphicsDevice.Viewport.Height) //Removes vegetable when it goes off the screen
            {
                Game.Components.Remove(this);
            }



            if ((position.X - texture.Width > Game.GraphicsDevice.Viewport.Width) || position.X + texture.Width < 0)
            {
                Game.Components.Remove(this);
            }
            Rectangle vegetableHitBox = texture.Bounds;
            vegetableHitBox.Location = this.position.ToPoint();


            MouseState mouseState = Mouse.GetState();
            var mousePosition = new Point(mouseState.X, mouseState.Y);

            if (vegetableHitBox.Contains(mousePosition))
            {
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    gameTools.UpdateScore(GameTools.FruitOrVegetable.Vegetable);
                    squish.Play();
                    Game.Components.Remove(this);
                }
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            squish = Game.Content.Load<SoundEffect>("Sounds/squish");
            base.LoadContent();
        }
    }
}
