﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectShell
{
    
    public class GameTools : DrawableGameComponent
    {

        /// <summary>
        /// Used when updating the score.
        /// Fruit increases points, while vegetables reduce points.
        /// </summary>
        public enum FruitOrVegetable
        {
            Fruit,
            Vegetable
        }
        
        GameOver gameOver;

        public int timeLeft = 60; //Playesr have 60 seconds to play
        double currentTime = 0; //Increases as the game goes on
        int score = 0;
        bool updatedScore = false; //Turns true if the score is updated.
        
        SpriteFont SFTimeLeft;
        SpriteFont SFScore;

        public GameTools(Game game, GameScene parent) : base(game)
        {
            if (game.Services.GetService<GameTools>() == null)
            {
                game.Services.AddService<GameTools>(this);
            }
            
        }

        /// <summary>
        /// Draws the time left in the top left, and the score in the top right.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = Game.Services.GetService<SpriteBatch>();

            sb.Begin();

            sb.DrawString(SFTimeLeft, "Time Left: " + timeLeft, new Vector2(20, 20), Color.White);
            sb.DrawString(SFScore, "Score: " + score, new Vector2(Game.GraphicsDevice.Viewport.Width - SFScore.MeasureString($"Score: {score}").X-20 , 20), Color.White);

            sb.End();
            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Every second currentTime increases, when it does the timeLeft in the game decreases.
        /// If the timeLeft is 0 the game is over.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            
            currentTime += gameTime.ElapsedGameTime.TotalSeconds;
            if (currentTime >= 1)
            {
                currentTime = 0;
                timeLeft--;
            }

            if (updatedScore == false)
            {
                if (timeLeft == 0)
                {
                    gameOver = Game.Services.GetService<GameOver>();
                    gameOver.SetGameToOver(score);
                    SaveScoreToFile hiScoreManager = new SaveScoreToFile();
                    hiScoreManager.LoadFile();
                    hiScoreManager.SaveScore(score);
                    updatedScore = true;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Loads the fonts to be used
        /// </summary>
        protected override void LoadContent()
        {
            SFTimeLeft = Game.Content.Load<SpriteFont>("Fonts\\TimeLeft");
            SFScore = Game.Content.Load<SpriteFont>("Fonts\\Score");
            base.LoadContent();
        }

        /// <summary>
        /// Updates the score according to what object was clicked.
        /// If a fruit is clicked the score is increased by 1.
        /// If a vegetable is clicked the score is decreased by 1.
        /// </summary>
        /// <param name="fruitOrVegetable"></param>
        public void UpdateScore(FruitOrVegetable fruitOrVegetable)
        {
            if(fruitOrVegetable == FruitOrVegetable.Fruit)
            {
                score++;
            }
            else if(fruitOrVegetable == FruitOrVegetable.Vegetable)
            {
                if (score > 0)
                {
                    score--;
                }
            }
        }
    }
}
