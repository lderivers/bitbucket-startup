﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FinalProjectShell
{
    class SaveScoreToFile
    {
        List<int> highScoreList = new List<int>(10);//High score list, maximum of 10 entries
        
        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory),"LDFinalProjectScore.txt"); //The file is placed on the desktop

        /// <summary>
        /// Gets the contents of the file and populates the highScoreList
        /// If the file doesnt exist already, one is created
        /// </summary>
        /// <returns></returns>
        public List<int> LoadFile()
        { 
            if (File.Exists(filePath))
            {
                List<string> fileContents = File.ReadAllLines(filePath).ToList();
                highScoreList = fileContents.Select(a=>int.Parse(a)).ToList();
                highScoreList = highScoreList.OrderByDescending(a => a).ToList();
                return highScoreList;
            }
            else
            {
                using (FileStream fs = File.Create(filePath)) { }
                
                List<string> fileContents = File.ReadAllLines(filePath).ToList();
                highScoreList = fileContents.Select(a => int.Parse(a)).ToList();
                highScoreList = highScoreList.OrderByDescending(a => a).ToList();
                return highScoreList;
                
            }
        }

        /// <summary>
        /// If the score is greater than one, the list is checked to see if less than 10 scores already exist.
        /// If there is less than 10 the score is automatically added.
        /// If there is 10 scores the lowest score is removed and the new score is placed accordingly in the list.
        /// </summary>
        /// <param name="score"></param>
        public void SaveScore(int score)
        {
            for(int i = 0; i < highScoreList.Count;i++)
            {
                if (score > 0)
                {

                    if (highScoreList.Count < 10)
                    {
                        highScoreList.Add(score);
                        highScoreList = highScoreList.OrderByDescending(a => a).ToList();

                        using (TextWriter writer = new StreamWriter(filePath))
                        {
                            foreach (int s in highScoreList)
                            {
                                writer.WriteLine(s);
                            }
                        }
                    }
                    else if (score >= i)
                    {
                        highScoreList.RemoveAt(9);

                        highScoreList.Add(score);
                        highScoreList = highScoreList.OrderByDescending(a => a).ToList();

                        using (TextWriter writer = new StreamWriter(filePath))
                        {
                            foreach (int s in highScoreList)
                            {
                                writer.WriteLine(s);
                            }
                            break;
                        }
                    }
                }
                
            }
        }
    }
}
